<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mozocloud' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345678' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#h.7OYDNA3etW=/%hCtDsl?)~hgnk!E66jCsn[Nk[)Acpo*BPXgzqi[qf{anbqeU' );
define( 'SECURE_AUTH_KEY',  'cCiI}+rk>Uz@,d^l^9ig7}*AsU+$)TAye?ApIm3%A.U7`m:h>&+r255K[<5xW&R*' );
define( 'LOGGED_IN_KEY',    'Xi0D]$W^{J+g_3^Xya9 o[clfues4I_  /Bdx&L+  >hTl_IP4+B#0gI@5dbKx^z' );
define( 'NONCE_KEY',        'O%FHuUayDXv-],ST+z7h((7P!mGLNDi_/QIYQn]0IdtvF9!,McF5q8Jz)NQYn)p[' );
define( 'AUTH_SALT',        'P%-KF9?aw<AN%[L]3p|VMVbN]/|576;$<T4<OIfwDKfCvpD*2v58V7=_mv1p-rQ9' );
define( 'SECURE_AUTH_SALT', '4$|Ilu%gSd)}/@.I@dNE]tLkQiJ$@BNPN+~oJvZ9-2i}dNGQOlpE!pLkm(uS9j;:' );
define( 'LOGGED_IN_SALT',   ',V-&;svI%oI[ss=WFv,ty)9,.eDhZ<kS</H_#!kL*ssq%.7L|pqn0$@lB$HLcUrf' );
define( 'NONCE_SALT',       '&1O6aXY9K[dk3UY36)g6L`/=eDCoaJPpx@J87Z]}V+F60E6qsF,xu!s|:h$PDs3Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
