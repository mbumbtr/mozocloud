<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    </div>
    <!-- .site-content -->

    <footer id="colophon" class="site-footer footer slice200" role="contentinfo">
        <div class="container">
            <div class="row">
                <!-- end col-12 -->
                <div class="col-lg-5 wow fadeInUp">
                    <a href="<?php echo get_site_url() ?>"> <img class="footer-logo mb-5"
                        src="<?php echo get_site_url() ?>/wp-content/uploads/mozo-logo.svg" alt="mozocloud"> </a>
                </div>
                <!-- end col-5 -->
                <div class="col-lg-4 col-md-6 wow fadeInUp">
                    <h5>Location</h5>
                    <address>
                    <p>2690, Ramsdell Pl., San Jose (CA), USA</p>
                    <p>603 Platinum Square, Pune (MH), India</p>
                    <p> Tower 61, Indore (MP), India <p> 

                </address>
                </div>
                <!-- end col-4 -->
                <div class="col-lg-3 col-md-6 wow fadeInUp">
                    <h5>Say Hello</h5>
                    <p>
                        <a href="mailto:hello@mozocloud.com">hello@mozocloud.com </a>
                    </p>
                    <p> +91 731-407-7574 </p>
                </div>
                <!-- end col-3 -->
                <div class="col-md-6 wow fadeInUp">
                    <div class="sub-footer">
                        <?php wp_nav_menu( array('menu'=>'top-menu')); ?>
                    </div>
                    <!-- end sub-footer -->
                </div>
                <div class="col-md-6 wow fadeInUp">
                    <div class="sub-footer text-right">
                        <p>&copy;
                            <?php echo date('Y'); ?> Deqode - All rights Reserved</p>
                    </div>
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </footer>
    </div>
    <!-- .site-inner -->
    </div>
    <!-- .site -->

    <?php wp_footer(); ?>
        </body>

        </html>