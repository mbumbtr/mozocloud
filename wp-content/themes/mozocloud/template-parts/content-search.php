<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php twentysixteen_post_thumbnail(); ?>

            <section class="slice">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="avatar mb-4">
                                by
                                <?php the_author(); ?>
                            </div>
                            <div>
                                <small class="post-date"> <time datetime="<?php echo get_the_date('c'); ?>"
                                itemprop="datePublished"><?php echo get_the_date(); ?></time></small>
                            </div>

                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                <?php twentysixteen_excerpt(); ?>
                        </div>
                    </div>
                </div>
            </section>

            <!-- 
	<?php if ( 'post' === get_post_type() ) : ?>

		<footer class="entry-footer">
			<?php twentysixteen_entry_meta(); ?>
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Post title. */
						__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer> 

	<?php else : ?> -->

            <?php
			edit_post_link(
				sprintf(
					/* translators: %s: Post title. */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<footer class="entry-footer"><span class="edit-link">',
				'</span></footer><!-- .entry-footer -->'
			);
		?>

                <?php endif; ?>
    </article>
    <!-- #post-<?php the_ID(); ?> -->