<?php
   /* Template Name: Home */
   get_header();
   ?>

    <div class="headlines">
        <div class="container">
            <h1>Your Cloud, <br> Your Way</h1>
        </div>
        <!-- end container -->
    </div>
    <!-- end headlines -->

    <?php include 'includes/case-studies.php' ?>

        <!-- end works -->
        <section class="featured-services slice-top200" data-color="dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="featured-inner-titles mb-5">
                            <h2>We make the cloud work for you</h2>
                            <p> Scale, save, secure. Get the most from the cloud with MozoCloud's advanced cloud services. </p>
                        </div>
                        <!-- end titles -->
                    </div>
                </div>
                    <div class="row">
                    <!-- end col-12 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="<?php echo get_site_url() ?>/devops">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/devops-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>01</span>
                                        <h4>DevOps</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/migration-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>02</span>
                                        <h4>Cloud Migration</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/native-apps-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>03</span>
                                        <h4>Cloud Native Apps</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/cloud-managed-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>04</span>
                                        <h4>Cloud Managed Services</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/robotic-process-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>05</span>
                                        <h4>Robotic Process Automation</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/google-cloud-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>06</span>
                                        <h4>Google Cloud Services</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/advisory services-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>07</span>
                                        <h4>Cloud Advisory Services</h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                    <div class="col-lg-3 col-md-6">
                        <li>
                            <a href="#">
                                <figure class="reveal-effect masker wow"> <img src="<?php echo get_site_url() ?>/wp-content/uploads/cloud-analytics-icon.svg" alt="mozocloud">
                                    <figcaption>
                                        <span>08</span>
                                        <h4>Cloud Analytics </h4>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </section>
        <!-- end featured-services -->

        <?php include 'includes/partnership.php' ?>
            <?php include 'includes/clients.php' ?>

                <!-- end recent-news -->

                <!-- end our-awards -->
                <!-- end showreel -->
                <!-- end logos -->
                <section class="work-with-us slice200" data-color="dark">
                    <div class="container wow fadeInUp">
                        <h6>Get a certified cloud consultant on the phone</h6>
                        <h2>Embrace the Cloud. Let’s Talk.</h2>
                        <a class="mozo-outline-btn contact-burger" href="javascript:void(0)"> Contact Us </a>
                    </div>
                    <!-- end container -->
                </section>

                <?php get_footer(); ?>