<?php
   /* Template Name: Case-Studies-Inner */
   get_header();
   ?>

    <div class="case-studies-inner">

        <div class="case-st-images" style="background:url(<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg)">
            <div class="container-fluid">
                <div class="col-lg-7">
                    <h1> Next-generation Smartphone connectivity through Google Cloud </h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-8 order-lg-1 order-2">
                    <div class="slice">
                        <h5> Using Google Cloud services such as Google Kubernetes Engine and Google BigQuery, Geoverse aims
                        at improving customer satisfaction by increasing the efficiency of services and reducing cost.
                    </h5>
                        <div class="my-4 bg-light p-4 ">
                            <h5 class="mb-4"><b> Google Cloud Results </b></h5>
                            <ul>
                                <li>GCP facilitated Geoverse with high scalability to adapt more quickly to behavioral changes of their end customers</li>
                                <li>Able to flourish and thrive in a competitive market</li>
                                <li>Saved 40% in cloud hosting costs</li>
                            </ul>
                        </div>

                        <p> Lack of seamless mobile coverage and poor user experience is unfortunately common in many indoor venues. Traditional indoor cellular systems are too costly and complex for enterprise deployment at smaller venues. Geoverse addresses these in-building coverage and capacity challenges with a revolutionary and cost-effective solution. Founded in 2017 in the United States, Geoverse is a mobile network operator leveraging over 30 years of cellular experience and international carrier relationships to offer private LTE networks. Geoverse expertise in providing reliable and secure 4G LTE connectivity with design, deployment, and operations and provides the solution bundle as a simple monthly subscription model.</p>

                        <p>Geoverse designs and operates private LTE networks that provide a secure, ultra-fast, and reliable internet connection throughout the building, increasing worker productivity, saving time, and improving business credibility. As a result, commercial real estate and enterprises can offer occupants 5-bar cellular service and high-performance data networks. The company's services are used in multiple verticals, including office buildings, logistics, retail, entertainment venues, government, healthcare, and education. Geoverse networks are upgradeable to 5G.</p>

                        <p>Geoverse utilizes blockchain technology in order to provide trust and immutability in its solution. End-to-end encryption on LTE provides greater security than enterprise Wi-Fi. The system has a certificate authority to manage cryptographically unique identities of suppliers and consumers participating in the network. Also, the permissioned blockchain solution based on Hyperledger Fabric restricts anyone from participating in the blockchain network.</p>

                        <p>With an increase in its customer base, Geoverse soon realized a challenge - difficulty to manage to provide reliable service and updates to millions of devices using an IT infrastructure built on individual virtual machines. Also, Geoverse had to provide a neutral host model simplifying high capacity and lightning-fast LTE network sharing with simple low-cost operation and deployment for In Building and Private wireless network, making wireless signals stronger and phone calls easier.</p>

                        <p>Geoverse sought advice from MozoCloud consultants and cloud architects to help them with their scaling issues. To increase their scaling, MozoCloud helped Geoverse migrate their services to Docker containers running on Google Kubernetes Engine—built on the open-source Kubernetes system. With Kubernetes Engine, Geoverse possesses a managed orchestration platform for Kubernetes which helped its small team focus on innovation instead of IT maintenance.</p>

                        <p>To store and manage the container images Geoverse used Container Registry and Cloud Memorystate as the database to hold and support secure static assets such as images and configuration files. Google Stackdriver gave Geoverse the capacity to monitor its system in depth. Separately, Geoverse ran the stateful services on Compute Engine instances which scale up and down automatically when required.</p>

                        <p>With their shift to Google Cloud Platform, the Geoverse team now had more time to innovate and experiment since there is no extra work for them in terms of managing the extensive environment thus allowing them to ship faster and to focus on more strategic projects. </p>

                        <p>Geoverse wanted to lessen their operating costs while maintaining high availability in addition to streamlining management. Google Cloud Platform helped Geoverse pave way for the same.</p>

                        <p>By using preemptible Google Compute Engine instances with automated container management for fault tolerance, Geoverse gets the best of both worlds: high availability and highly affordable compute instances. This helped Geoverse lower its operating cost and manage the identity and access management of the suppliers and consumers participating in the system in a better way.
                        </p>

                        <p>Also, Geoverse makes use of Google Cloud SQL, Google’s native fully managed database services which helped it free its engineers from redundant performance optimization and operational tasks. Cloud SQL helped Geoverse technicians to improve their existent development cycles and earn extra time to work on projects that provide value to its customers.</p>

                        <p>Geoverse also incorporated Cloud NAT - a GCP-managed high-performance Network Address Translation to handle deployment and also to maintain the privacy of the offer negotiations and contract terms.
                        </p>

                        <p>With Google Cloud Platform and Kubernetes Engine, Geoverse is on its way to innovate and explore further in their niche high capacity and lightning-fast LTE network. </p>

                    </div>
                </div>
                <div class="col-lg-4 order-lg-2 order-1">
                    <div class="case-st-about ">
                        <div class="mb-4">
                            <img class="case-st-about-logo" src="<?php echo get_site_url() ?>/wp-content/uploads/Geoverse-Logo.png">
                        </div>
                        <div class="mb-4">
                            <h4 class="headline-big"> About <span> METRO </span> </h4>
                        </div>
                        <div class="mb-4">
                            <p class="about-paragraph"> Geoverse, a subsidiary of ATN International, founded in 2017, is a mobile network operator that provides private cellular network solutions to various enterprises as services. Geoverse has a long term experience and international carrier relationships of over 30 years in this business. Geoverse uses CBRS and a licensed carrier spectrum to build a solution that connects people, things, and data in a cost-effective way that can leverage the upcoming technologies as well, such as 5G. </p>
                        </div>
                        <div class="mb-4">
                            <p class="case-st-industries"> Industries: <span> Telecommunications </span> </p>
                        </div>
                        <div class="mb-4">
                            <p class="case-st-industries"> Location: <span> USA </span> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php get_footer(); ?>