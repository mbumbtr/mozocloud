<?php
   /* Template Name: Case-Studies */
   get_header();
   ?>
    <!-- end headlines -->

    <div class="headlines">
        <div class="container">
            <h1> Case Studies </h1>
        </div>
        <!-- end container -->
    </div>

    <?php include 'includes/case-studies.php' ?>

        <section class="slice100 casestudies">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 casestudies-box">
                        <div class="project-box ">
                            <figure class="project-image reveal-effect masker wow">
                                <a href="<?php echo get_site_url() ?>/case-studies-inner"><img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg" alt="Image"
                                class=""></a>
                            </figure>
                            <div class="project-content">
                                <h4> Geoverse </h4>
                                <h3><a href="<?php echo get_site_url() ?>/case-studies-inner"> Tokenizing Real Estate Assets
                            </a></h3>
                                <p>Learn how Deqode helped its customer to attract a pool of global investors...</p>
                                <a href="<?php echo get_site_url() ?>/case-studies-inner" class="btn-link">SEE CASE STUDY</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 casestudies-box">
                        <div class="project-box ">
                            <figure class="project-image reveal-effect masker wow">
                                <a href="<?php echo get_site_url() ?>/case-studies-inner"><img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg" alt="Image"
                                class=""></a>
                            </figure>
                            <div class="project-content">
                                <h4> Geoverse </h4>
                                <h3><a href="<?php echo get_site_url() ?>/case-studies-inner"> Tokenizing Real Estate Assets
                            </a></h3>
                                <p>Learn how Deqode helped its customer to attract a pool of global investors...</p>
                                <a href="<?php echo get_site_url() ?>/case-studies-inner" class="btn-link">SEE CASE STUDY</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 casestudies-box">
                        <div class="project-box">
                            <figure class="project-image reveal-effect masker wow">
                                <a href="<?php echo get_site_url() ?>/case-studies-inner"><img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg" alt="Image"
                                class=""></a>
                            </figure>
                            <div class="project-content">
                                <h4> Geoverse </h4>
                                <h3><a href="<?php echo get_site_url() ?>/case-studies-inner"> Tokenizing Real Estate Assets
                            </a></h3>
                                <p>Learn how Deqode helped its customer to attract a pool of global investors...</p>
                                <a href="<?php echo get_site_url() ?>/case-studies-inner" class="btn-link">SEE CASE STUDY</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 casestudies-box">
                        <div class="project-box">
                            <figure class="project-image reveal-effect masker wow">
                                <a href="<?php echo get_site_url() ?>/case-studies-inner"><img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg" alt="Image"
                                class=""></a>
                            </figure>
                            <div class="project-content">
                                <h4> Geoverse </h4>
                                <h3><a href="<?php echo get_site_url() ?>/case-studies-inner"> Tokenizing Real Estate Assets
                            </a></h3>
                                <p>Learn how Deqode helped its customer to attract a pool of global investors...</p>
                                <a href="<?php echo get_site_url() ?>/case-studies-inner/" class="btn-link">SEE CASE STUDY</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php get_footer(); ?>