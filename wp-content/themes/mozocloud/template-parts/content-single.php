<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

    <div class="headlines">
        <div class="container">
            <h1 class="page-title screen-reader-text">Blog</h1>
            <?php the_title( '<h2 class="entry-title inner-entry-title mt-4">', '</h2>' ); ?>
        </div>
        <!-- end container -->
    </div>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">

        </header>
        <!-- .entry-header -->

        <?php twentysixteen_excerpt(); ?>

            <?php twentysixteen_post_thumbnail(); ?>
                <section class="slice">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="avatar mb-4">
                                    by
                                    <?php the_author(); ?>
                                </div>

                                <div>
                                    <small class="post-date"> <time datetime="<?php echo get_the_date('c'); ?>"
                                itemprop="datePublished"><?php echo get_the_date(); ?></time></small>
                                </div>

                                <div class="entry-content">
                                    <?php
			the_content();

			wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			);

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
			?>
                                </div>
                                <!-- .entry-content -->
                            </div>
                        </div>
                    </div>
                </section>

                <footer class="entry-footer">

                    <?php
			edit_post_link(
				sprintf(
					/* translators: %s: Post title. */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
                </footer>
                <!-- .entry-footer -->
    </article>
    <!-- #post-<?php the_ID(); ?> -->