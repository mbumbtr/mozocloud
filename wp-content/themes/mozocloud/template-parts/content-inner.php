<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?> 
<!-- end page-header -->
<section class="blog slice100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="post wow fadeIn inner-blog">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <section class="page-header">
                            <div class="post-sticky wow fadeIn">
                                <figure class="post-image"> <?php twentysixteen_post_thumbnail(); ?> </figure>
                                <div class="post-content">
                                    <div class="avatar">
                                        by <?php the_author(); ?>
                                    </div>
                                    <?php the_title( sprintf( '<h3 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
                                    <small class="post-date"> <time datetime="<?php echo get_the_date('c'); ?>"
                                            itemprop="datePublished"><?php echo get_the_date(); ?></time></small>
                                    <div class="entry-content">
                                        <?php
                                        echo substr(strip_tags($post->post_content), 0, '150').'...'; 
 
                                wp_link_pages(
                                    array(
                                        'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
                                        'after'       => '</div>',
                                        'link_before' => '<span>',
                                        'link_after'  => '</span>',
                                        'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
                                        'separator'   => '<span class="screen-reader-text">, </span>',
                                    )
                                );
                                ?>
                                    </div> 
                                    <a href="<?php echo get_permalink( $post->ID ); ?>" class="post-link">READ MORE</a>
                                </div>
                                <!-- end post-content -->
                            </div>
                            <!-- end post-sticky -->
                        </section>
                    </article> 
                </div>
                <!-- end post -->
            </div>
            <!-- end col-8 -->
            <div class="col-lg-4">              
					<?php get_sidebar(); ?>                   
            </div>
            <!-- end col-4-->
        </div>
    </div>
</section> 