<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article class="mb-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <section class="page-header">
        <div class="post-sticky wow fadeIn">
            <figure class="post-image"> <?php twentysixteen_post_thumbnail(); ?> </figure>
            <div class="post-content">
                <div class="avatar">
                    by <?php the_author(); ?>
                </div>
                <?php the_title( sprintf( '<h3 class="post-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
                <small class="post-date"> <time datetime="<?php echo get_the_date('c'); ?>"
                        itemprop="datePublished"><?php echo get_the_date(); ?></time></small>
                <div class="entry-content">
                <?php  
                    echo substr(strip_tags($post->post_content), 0, '150').'...'; 
			// the_content(
			// 	sprintf(
			// 		/* translators: %s: Post title. */
			// 		__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
			// 		get_the_title()
			// 	)
			// );

			wp_link_pages(
				array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				)
			);
			?>
                </div> 
                <a href="<?php echo get_permalink( $post->ID ); ?>" class="post-link">READ MORE</a>
            </div>
            <!-- end post-content -->
        </div>
        <!-- end post-sticky -->
    </section>
</article> 
 