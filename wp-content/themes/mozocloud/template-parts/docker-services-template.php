<?php
   /* Template Name: Docker-Services */
   get_header();
   ?>

<div class="headlines">
    <div class="container">
        <div class="col-md-12">
            <h1>DevOps as a Service</h1>
            <h2 class="inner-entry-title"> For speeding up deployments, better security, and cost savings </h2>
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end headlines -->

<section class="bg-light slice">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <div class="inner-title" >
                <h2> Docker Who </h2>
            </div>
                <p> In the old days, applications were run on Virtual Machines. These machines took up a lot of
                    resources and were slow because they had a lot of overhead (hypervisor, guest OS, and more). </p>
                <p> 2013 marked the release of Docker - a lightweight container-based technology, where containers had
                    direct access to host OS resources through the docker engine. This meant that containers could spin
                    up and spin down very quickly.</p>
                <p> Docker services are exciting because the technology makes the application more portable, changing
                    the cornerstone of how IT used to be. With Docker, instead of virtualizing the hardware and
                    software, you virtualize the hardware and give the software a very rigidly defined set of interfaces
                    that make it easier to make things that scale, are redundant, and resilient.</p>
            </div>
        </div>
    </div>
</section>

<section class="slice100 countries">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="inner-title" >
                <h2> Why Docker? </h2>
                <p> Docker helps with: </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Portability </h4>
                    <p> Once the containerized application is tested, one can deploy it to a completely different system
                        where Docker is running. The application will perform exactly as it did during testing. </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Cost Savings </h4>
                    <p> With Docker, fewer resources are required to run the application. Hence, companies can save
                        everything from server costs to the maintenance workforce.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Better Performance </h4>
                    <p> Containers do not contain an operating system, whereas VMs do. Hence, Docker containers have
                        much smaller footprints than VMs, need smaller development cycles, and are quicker to start.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Scalability </h4>
                    <p> It is lightning fast to create new containers whenever demand arises. With Docker, one can take
                        advantage of a range of container management options when using multiple containers. </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="slice bg-light countries">
    <div class="container"> 
        <div class="row mb-5 inner-title">
            <div class="col-md-12">
                <h2> Mozo’s Docker Services </h2>
                <p> MozoCloud helps you with: </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Docker Consulting </h4>
                    <p> We help you leverage Docker, and revamp your technical ecosystem by mapping a balanced
                        implementation of the same. </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Docker Implementation </h4>
                    <p> Using our rich experience with Docker, we transform your monolithic application into a modular,
                        containerized app.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Hiring Docker Developers </h4>
                    <p> We work as your extended team, where you could hire our docker developers and use their services
                        via time and material engagement. </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inner-services">    
                <h4> Docker Maintenance and Support </h4>
                    <p> We help you manage your growing containers with powerful platforms like Docker Swarm and
                        Kubernetes. </p>
                </div>
            </div>
        </div>
    </div>
</section>



<?php include 'includes/partnership.php' ?>
<?php include 'includes/clients.php' ?>
<!-- end logos -->
<section class="work-with-us" data-color="dark">
    <div class="container wow fadeInUp">
        <h6>Get a certified cloud consultant on the phone</h6>
        <h2>Embrace the Cloud. Let’s Talk.</h2>
        <a class="mozo-outline-btn contact-burger" href="javascript:void(0)"> Contact Us </a>
    </div>
    <!-- end container -->
</section>

<?php get_footer(); ?>