<?php
   /* Template Name: About */
   get_header();
   ?>

<div class="headlines">
    <div class="container">
        <div class="col-md-12">
            <h1>About MozoCloud </h1>
            <h2 class="inner-entry-title"> Find out more about Mozo’s journey, team, and work </h2>
        </div>
    </div>
    <!-- end container -->
</div>

<section class="about-intro slice bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Heads in the cloud, feet on the ground</h2>
                <p class="w6 mb-5" >Business is better in the cloud when it’s done with some Mozo. Our mission has been to make your cloud journey a hassle-free process.
                </p>
            </div>
        </div>
        <div class="row">
            <!-- end col-12 -->
            <div class="col-md-6">
                <p>
                MozoCloud is a subsidiary of Deqode, one of the premier blockchain companies around the globe. Mozo started when we found out that cloud wasn't just another 'technical requirement' that our customers need - rather it was a technical necessity. Working on some of the biggest projects in crypto in the past 5 years, we found out that every one of them required dedicated cloud experts, which we slowly began housing. Our expertise was so overwhelming, that we started a separate cloud wing - which slowly evolved into MozoCloud. </p>
            </div>
            <!-- end col-6 -->
            <div class="col-md-6">
                <p>

                At MozoCloud, our purpose has been to make the cloud easy. All we do is bring your vision to reality - and put it on the cloud. After all, it's your cloud: it should be your way. </p>
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>


<section class="team slice-top200" data-color="dark">
  <div class="container">
   <div class="row">
   <div class="col-md-12">
   		<div class="titles">
      <h6>Leadership</h6>
    </div>
   </div>
   </div>

   <div class="row">
   	<div class="col-lg-4 col-md-6">
   		<figure>
   			<img src="<?php echo get_site_url() ?>/wp-content/uploads/ankur.jpg" alt="Ankur Maheshwari">
   			<figcaption>
   			<h5>Ankur Maheshwari</h5>
   				<span>Chief Executive Officer</span>
   				<a target="_blank" href="https://www.linkedin.com/in/ankur-maheshwari-9ab2a215/">Linkedin</a>
   			</figcaption>
   		</figure>
   	</div>
   	<!-- end col-4 -->
   	<div class="col-lg-4 col-md-6">
   		<figure>
   			<img src="<?php echo get_site_url() ?>/wp-content/uploads/latha-sharma.jpg" alt="Latha Sharma">
   			<figcaption>
   			<h5>Latha Sharma</h5>
   				<span>Chief Marketing Officer</span>
   				<a target="_blank" href="https://www.linkedin.com/in/lathasharma1482/">Linkedin</a>
   			</figcaption>
   		</figure>
   	</div>
   	<!-- end col-3 -->
   </div>
   <!-- end row -->
  </div>
  <!-- end container --> 
</section>

<?php include 'includes/partnership.php' ?>
<?php include 'includes/clients.php' ?>
<!-- end logos -->
<section class="work-with-us" data-color="dark">
    <div class="container wow fadeInUp">
        <h6>Get a certified cloud consultant on the phone</h6>
        <h2>Embrace the Cloud. Let’s Talk.</h2>
        <a class="mozo-outline-btn contact-burger" href="javascript:void(0)"> Contact Us </a>
    </div>
    <!-- end container -->
</section>

<?php get_footer(); ?>