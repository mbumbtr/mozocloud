<!-- end header -->
<section class="slider slicem-bottom">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background:url(<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg)">
                <div class="slide-inner">
                    <h4> Geoverse </h4>
                    <h2>How Mozo Cut Down Telecom Giant Geoverse's Infra Cost by 40%</h2>
                    <div class="link">
                        <a href="<?php echo get_site_url() ?>/case-studies-inner">SEE CASE STUDY</a>
                    </div>
                    <!-- end link -->
                </div>
                <!-- end slide-inner -->
            </div>
            <!-- end swiper-slide -->
            <div class="swiper-slide" style="background:url(<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg)">
                <div class="slide-inner">
                    <h4> Geoverse </h4>
                    <h2>How Mozo Cut Down Telecom Giant Geoverse's Infra Cost by 40%</h2>
                    <div class="link">
                        <a href="<?php echo get_site_url() ?>/case-studies-inner">SEE CASE STUDY</a>
                    </div>
                    <!-- end link -->
                </div>
                <!-- end slide-inner -->
            </div>
            <!-- end swiper-slide -->
            <div class="swiper-slide" style="background:url(<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg)">
                <div class="slide-inner">
                    <h4> Geoverse </h4>
                    <h2>How Mozo Cut Down Telecom Giant Geoverse's Infra Cost by 40%</h2>
                    <div class="link">
                        <a href="<?php echo get_site_url() ?>/case-studies-inner">SEE CASE STUDY</a>
                    </div>
                    <!-- end link -->
                </div>
                <!-- end slide-inner -->

            </div>
            <!-- end swiper-slide -->
        </div>
        <!-- end swiper-wrapper -->
        <div class="swiper-pagination"></div>
        <!-- end swiper-pagination -->
        <div class="swiper-button-next"><span></span><b>NEXT</b></div>
        <!-- end swiper-button-next -->
    </div>
    <!-- end swiper-container -->
</section>