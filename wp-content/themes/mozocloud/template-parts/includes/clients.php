<section class="logos slice200">
    <div class="container">
        <div class="titles">
            <h6>Clients</h6>
            <p>Trusted by the best in the industry</p>
        </div>
        <!-- end titles -->

        <div class="row">
            <div class="col-md-3 col-6 client-border  ">
                <div class="client-logo wow fadeInUp">
                    <a href="https://rokk3r.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/rokk3r.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://fandomsports.net/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/fandom-dark.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://www.geoverse.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/geo-dark.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://peculium.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/peculium.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://www.monetha.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/monetha-dark.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://verasity.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/verasity-dark.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://www.pumapay.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/pumapay-dark.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="http://gaed2.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/gaed.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://blockapps.net/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/blockapps.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://www.i-fashiongroup.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/fashiongroup.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://cannacor.io/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/cannacor.png" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://investinseychelles.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/sib.png" alt="mozocloud">
                    </a></div>
            </div>

        </div>

    </div>
    <!-- end container -->
</section>