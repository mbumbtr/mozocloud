<section class="our-awards slice200">
    <div class="container">
        <div class="titles">
            <h6>Partners</h6>
            <p> Partnered with the best in the industry </p>
        </div>
        <!-- end titles -->
        <div class="row">
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://cloud.google.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/google-cloud.svg" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://azure.microsoft.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/microsoft-azure.svg" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://www.cloudera.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/cloudera.svg" alt="mozocloud">
                    </a></div>
            </div>
            <div class="col-md-3 col-6 client-border">
                <div class="client-logo wow fadeInUp">
                    <a href="https://aws.amazon.com/" target="_blank">
                        <img src="<?php echo get_site_url() ?>/wp-content/uploads/aws.svg" alt="mozocloud">
                    </a></div>
            </div>

        </div>
    </div>
    <!-- end container -->
</section>