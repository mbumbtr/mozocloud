<?php
   /* Template Name: DevOps */
   get_header();
   ?>

<div class="headlines">
    <div class="container">
        <div class="col-md-12">
            <h1>DevOps as a Service</h1>
            <h2 class="inner-entry-title"> For faster time-to-market, higher efficiency, and lower costs </h2>
        </div>
    </div>
    <!-- end container -->
</div>
<!-- end headlines -->

<section class="main-page-bg slicem-bottom"
    style="background:url(<?php echo get_site_url() ?>/wp-content/uploads/blog2.jpg)">
    <div class="container-fluid ">
        <div class="row">
            <div class="col-md-8">
                <h2> Accelerated DevOps </h2>
                <p>Mozo's DevOps as a Service offering is based on a flexible and powerful DevOps and DevSecOps pipeline, surrounded by expert-level development services and an ecosystem of industry-standard tools. We help you build fast, secure and flexible operations that adapt to the turbulent market by leveraging continuous integration, continuous delivery, reusable workloads, and heavily reduced costs.</p>
            </div>
        </div>
    </div>
</section>

<section class="slice100 countries">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="about-countries">
                    <h2> 7+ </h2>
                    <p> 7+ Years in DevOps </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about-countries">
                    <h2> 100+ </h2>
                    <p> DevOps Engineers </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="about-countries">
                    <h2> 10+ </h2>
                    <p> DevOps Implementations </p>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- end works -->
<section class="featured-services slice-top100" data-color="dark">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="featured-inner-titles slicem-bottom">
                    <h2>MozoCloud's DevOps as a Service expertise includes</h2>
                </div>
                <!-- end titles -->
            </div>
        </div>
        <div class="row">
            <!-- end col-12 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="<?php echo get_site_url() ?>/devops">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/devops-icon.svg" alt="mozocloud">
                            <figcaption>
                                <span>01</span>
                                <h4>End-to-End DevOps Services</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="#">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/migration-icon.svg"
                                alt="mozocloud">
                            <figcaption>
                                <span>02</span>
                                <h4>Continuous Integration and Continuous Development</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="#">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/native-apps-icon.svg"
                                alt="mozocloud">
                            <figcaption>
                                <span>03</span>
                                <h4>Continuous Performance Testingv</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="#">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/cloud-managed-icon.svg"
                                alt="mozocloud">
                            <figcaption>
                                <span>04</span>
                                <h4>Infrastructure as Code</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="#">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/robotic-process-icon.svg"
                                alt="mozocloud">
                            <figcaption>
                                <span>05</span>
                                <h4>Robotic Process Automation</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
            <div class="col-lg-3 col-md-6">
                <li>
                    <a href="#">
                        <figure class="reveal-effect masker wow"> <img
                                src="<?php echo get_site_url() ?>/wp-content/uploads/google-cloud-icon.svg"
                                alt="mozocloud">
                            <figcaption>
                                <span>06</span>
                                <h4>Infrastructure as Code</h4>
                            </figcaption>
                        </figure>
                    </a>
                </li>
            </div>
            <!-- end col-3 -->
        </div>
        <div class="row my-5">
            <div class="col-md-12">
                <p class="my-4 w6"> Looking to <a class="inner-link-white" href="#"> Build Cloud Native Apps </a>
                    instead?</p>
            </div>
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>


<section class="slice200">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-page-heading">
                    <h2> Automation in the Cloud </h2>
                    <p> From managing DevOps practices, automation and process transformation to complete management of
                        the
                        delivery pipeline, MozoCloud's DevOps as a Service takes care of it all. </p>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="our-cloud">
                    <img class="cloud-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/MozoAssess.svg" alt="mozocloud">
                    <p> Our DevOps experts assess your practices, map out existing DevOps practice and development
                        pipeline, and identify the right set of tools. Post this, we produce an assessment report and a
                        list of activities required to automate development and operations.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="our-cloud">
                    <img class="cloud-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/MozoBuild.svg" alt="mozocloud">
                    <p> After drawing the ideal workflows, we integrate cloud-native technologies and build systems that
                        help to automate existing workloads in a new environment. We prevent risks and cut costs using
                        our tested ecosystem of open-sourced and licensed tools.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="our-cloud">
                    <img class="cloud-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/MozoManage.svg" alt="mozocloud">
                    <p> Once your DevOps transformation gets completed, our team provides you with ongoing cloud
                        management and support. Our experts become an extension of your team and help you achieve higher
                        development agility and smaller delivery cycles.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'includes/case-studies.php' ?>
<section class="slice100 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-page-heading">
                    <h2> Mozo’s DevOps Toolkit </h2>
                </div>
            </div>
        </div>
        <div class="other-tools">
            <div class="row justify-content-md-center">
                <div class="col-md-3 col-6">
                    <a href="<?php echo get_site_url() ?>/docker-services/">
                        <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/docker.svg" alt="mozocloud">
                    </a>
                </div>
                <div class="col-md-3 col-6">
                    <a href="#">
                        <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/kubernetes.svg" alt="mozocloud">
                    </a>
                </div>
                <div class="col-md-3 col-6">
                    <a href="#">
                        <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/terraform.svg" alt="mozocloud">
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


<section class="slice100 ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-page-heading">
                    <h2> Other Tools </h2>
                </div>
            </div>
        </div>
        <div class="row mt-5">

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Source Code Management </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/svn-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/git-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/bitbucket-blue.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/github.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Build </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/apache-ant-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/maven-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/grunt-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/gradle-logo.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Configuration Management </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/puppet-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/chef-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/ansible-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/saltstack-ar.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/terraform.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/vagrantup-ar.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Continuous Integration </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/jenkins.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/travis-ci.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/bamboo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/hudson-logo.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Continuous Security </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/snyk.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/netsparker.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Testing </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/selenium.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/testng.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/junit5.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Monitoring </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/nagios-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/sensuio-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/grafana.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/relic-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/elastic-logo.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/datadoghq.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="inner-page-heading">
                    <h4 class="w6"> Containerization/Orchestration </h4>
                </div>
                <div class="other-tools">
                    <div class="row">
                        <div class="col-6">
                            <img class="toolkit-icon" src="<?php echo get_site_url() ?>/wp-content/uploads/docker.svg" alt="mozocloud">
                        </div>
                        <div class="col-6">
                            <img class="toolkit-icon"
                                src="<?php echo get_site_url() ?>/wp-content/uploads/kubernetes.svg" alt="mozocloud">
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    </div>
</section>

<?php include 'includes/partnership.php' ?>
<?php include 'includes/clients.php' ?>
<!-- end logos -->
<section class="work-with-us" data-color="dark">
    <div class="container wow fadeInUp">
        <h6>Get a certified cloud consultant on the phone</h6>
        <h2>Embrace the Cloud. Let’s Talk.</h2>
        <a class="mozo-outline-btn contact-burger" href="javascript:void(0)"> Contact Us </a>
    </div>
    <!-- end container -->
</section>

<?php get_footer(); ?>