(function(jQuery) {
	jQuery(document).ready(function() {
		"use strict";
		

		
	// HAMBURGER AUDIO
			document.getElementById("hamburger-menu").addEventListener('click', function(e) {
	  	});
		
	// LOGO RANDOM FADE
		jQuery(function(){
		  // time between image rotate
		  var delay = 3000;

		  jQuery('.logos ul > li figure').each(function(){
			// save images in an array
			var jQueryimgArr = jQuery(this).children();
			// show a random image
			jQueryimgArr.eq(Math.floor(Math.random()*jQueryimgArr.length)).show();
		  });
		  // run the changeImage function after every (delay) miliseconds
		  setInterval(function(){
			changeImage();
		  },delay);

		  function changeImage(){
			// save list items in an array
			var jQueryliArr = jQuery('.logos ul > li figure');
			// select a random list item
			var jQuerycurrLi = jQueryliArr.eq(Math.floor(Math.random()*jQueryliArr.length));
			// get the currently visible image
			var jQuerycurrImg = jQuery('img:visible', jQuerycurrLi);
			if (jQuerycurrImg.next().length == 1) {
			  var jQuerynext = jQuerycurrImg.next();
			} else {
			  var jQuerynext = jQuery('img:first', jQuerycurrLi);
			}
			jQuerycurrImg.fadeOut( 1500 );
			jQuerynext.fadeIn( 1500 );
		  }  
		});
		
		
		
		// CONTACT FORM INPUT LABEL
			function checkForInput(element) {
			  const jQuerylabel = jQuery(element).siblings('span');
			  if (jQuery(element).val().length > 0) {
				jQuerylabel.addClass('label-up');
			  } else {
				jQuerylabel.removeClass('label-up');
			  }
			}

			// The lines below are executed on page load
			jQuery('input, textarea').each(function() {
			  checkForInput(this);
			});

			// The lines below (inside) are executed on change & keyup
			jQuery('input, textarea').on('change keyup', function() {
			  checkForInput(this);  
			});
		
		
		
		
		// SWIPER SLIDER
			var mySwiper = new Swiper ('.swiper-container', {
			slidesPerView: 'auto',
      		spaceBetween: 0,
			loop: true,
			autoplay: {
				delay: 6000,
				disableOnInteraction: false,
			  },
			pagination: {
				el: '.swiper-pagination',
					clickable: true,
				renderBullet: function (index, className) {
				  return '<span class="' + className + '"><svg><circle r="18" cx="20" cy="20"></circle></svg></span>';
				},
			},
			navigation: {
			  nextEl: '.swiper-button-next',
			  prevEl: '.swiper-button-prev',
			},
		  })
		
		
		
			
			
		// PAGE TRANSITION
		jQuery('body li a').on('click', function(e) {

			if (typeof jQuery( this ).data('fancybox') == 'undefined') {
			e.preventDefault(); 
			var url = this.getAttribute("href"); 
			if( url.indexOf('#') != -1 ) {
			var hash = url.substring(url.indexOf('#'));

			if( jQuery('body ' + hash ).length != 0 ){
			jQuery('.transition-overlay').removeClass("active");
			jQuery(".hamburger").toggleClass("open");
			jQuery("body").toggleClass("overflow");
			jQuery(".navigation-menu").removeClass("active");
			jQuery(".navigation-menu .inner ul").css("transition-delay", "0s");
			jQuery(".navigation-menu .inner blockquote").css("transition-delay", "0s");
			jQuery(".navigation-menu .bg-layers span").css("transition-delay", "0.2s");

			jQuery('html, body').animate({
			scrollTop: jQuery(hash).offset().top
			}, 1000);

			}
			}
			else {
			jQuery('.transition-overlay').toggleClass("active");
			setTimeout(function(){
			window.location = url;
			},600); 

			}
			}
			});
		
		
		
		
		
		// GO TO TOP
			jQuery(window).scroll(function () {
				if (jQuery(this).scrollTop() > 300) {
					jQuery('.gotop').fadeIn();
				} else {
					jQuery('.gotop').fadeOut();
				}
			});

			jQuery('.gotop').on('click', function(e) {
				jQuery("html, body").animate({
					scrollTop: 0
				}, 600);
				return false;
			});
		
		
		
		
		
		// STICKY SIDE LOGO
		jQuery(window).on("scroll touchmove", function () {
		jQuery('.logo').toggleClass('sticky', jQuery(document).scrollTop() > 300);
		});
		
		
		
		
		// HIDE NAVBAR
		jQuery(window).on("scroll touchmove", function () {
		jQuery('.navbar').toggleClass('hide', jQuery(document).scrollTop() > 30);
		});
		
		
		
		
		// DATA BACKGROUND IMAGE
			var pageSection = jQuery(".swiper-slide");
			pageSection.each(function(indx){
				if (jQuery(this).attr("data-background")){
					jQuery(this).css("background-image", "url(" + jQuery(this).data("background") + ")");
				}
			});
		
		
		
		
		// HAMBURGER
		jQuery(function(){
			var jQueryburger = jQuery('.burger');
			var jQueryburgerContact = jQuery('.contact-burger');
			var jQuerybars = jQuery('.burger-svg__bars');
			var jQuerybar = jQuery('.burger-svg__bar');
			var jQuerybar1 = jQuery('.burger-svg__bar-1');
			var jQuerybar2 = jQuery('.burger-svg__bar-2');
			var jQuerybar3 = jQuery('.burger-svg__bar-3');
			var isChangingState = false;
			var isOpen = false;
			var burgerTL = new TimelineMax();

			function burgerOver() {

				if(!isChangingState) {
					burgerTL.clear();
					if(!isOpen) {
						burgerTL.to(jQuerybar1, 0.5, { y: -2, ease: Elastic.easeOut })
								.to(jQuerybar2, 0.5, { scaleX: 0.6, ease: Elastic.easeOut, transformOrigin: "50% 50%" }, "-=0.5")
								.to(jQuerybar3, 0.5, { y: 2, ease: Elastic.easeOut }, "-=0.5");
					}
					else {
						burgerTL.to(jQuerybar1, 0.5, { scaleX: 1.2, ease: Elastic.easeOut })
								.to(jQuerybar3, 0.5, { scaleX: 1.2, ease: Elastic.easeOut }, "-=0.5");
					}
				}
			}

			function burgerOut() {
				if(!isChangingState) {
					burgerTL.clear();
					if(!isOpen) {
						burgerTL.to(jQuerybar1, 0.5, { y: 0, ease: Elastic.easeOut })
								.to(jQuerybar2, 0.5, { scaleX: 1, ease: Elastic.easeOut, transformOrigin: "50% 50%" }, "-=0.5")
								.to(jQuerybar3, 0.5, { y: 0, ease: Elastic.easeOut }, "-=0.5");
					}
					else {
						burgerTL.to(jQuerybar1, 0.5, { scaleX: 1, ease: Elastic.easeOut })
								.to(jQuerybar3, 0.5, { scaleX: 1, ease: Elastic.easeOut }, "-=0.5");
					}
				}
			}

			function showCloseBurger() {
				burgerTL.clear();
				burgerTL.to(jQuerybar1, 0.3, { y: 6, ease: Power4.easeIn })
						.to(jQuerybar2, 0.3, { scaleX: 1, ease: Power4.easeIn }, "-=0.3")
						.to(jQuerybar3, 0.3, { y: -6, ease: Power4.easeIn }, "-=0.3")
						.to(jQuerybar1, 0.5, { rotation: 45, ease: Elastic.easeOut, transformOrigin: "50% 50%" })
						.set(jQuerybar2, { opacity: 0, immediateRender: false }, "-=0.5")
						.to(jQuerybar3, 0.5, { rotation: -45, ease: Elastic.easeOut, transformOrigin: "50% 50%", onComplete: function() { isChangingState = false; isOpen = true; } }, "-=0.5");
			}

			function showOpenBurger() {
				burgerTL.clear();
				burgerTL.to(jQuerybar1, 0.3, { scaleX: 0, ease: Back.easeIn })
						.to(jQuerybar3, 0.3, { scaleX: 0, ease: Back.easeIn }, "-=0.3")
						.set(jQuerybar1, { rotation: 0, y: 0 })
						.set(jQuerybar2, { scaleX: 0, opacity: 1 })
						.set(jQuerybar3, { rotation: 0, y: 0 })
						.to(jQuerybar2, 0.5, { scaleX: 1, ease: Elastic.easeOut })
						.to(jQuerybar1, 0.5, { scaleX: 1, ease: Elastic.easeOut }, "-=0.4")
						.to(jQuerybar3, 0.5, { scaleX: 1, ease: Elastic.easeOut, onComplete: function() { isChangingState = false; isOpen = false; } }, "-=0.5");
			}

			jQueryburger.on('click', function(e) {
				jQuery("body").toggleClass("overflow");
				jQuery(".navigation-menu").toggleClass("active");
				jQuery(".navbar").toggleClass("light");
				if(!isChangingState) {
					isChangingState = true;

					if(!isOpen) {
						showCloseBurger();
					}
					else {
						showOpenBurger();
					}	
				}

			});
			
			jQueryburger.hover( burgerOver, burgerOut );

				jQueryburgerContact.on('click', function(e) {
				jQuery("body").toggleClass("overflow");
				jQuery(".contact-form-animation").toggleClass("active");
				jQuery(".navbar").toggleClass("light");
			});


		});
		
    	});
	
	
		// SCROLL BG COLOR
		jQuery(window).scroll(function() {
		  var jQuerywindow = jQuery(window),
			  jQuerybody = jQuery('body'),
			  jQuerypanel = jQuery('section, footer, header');

		  var scroll = jQuerywindow.scrollTop() + (jQuerywindow.height() / 3);

		  jQuerypanel.each(function () {
			var jQuerythis = jQuery(this);
			if (jQuerythis.position().top <= scroll && jQuerythis.position().top + jQuerythis.height() > scroll) {

			  jQuerybody.removeClass(function (index, css) {
				return (css.match (/(^|\s)color-\S+/g) || []).join(' ');
			  });

			  jQuerybody.addClass('color-' + jQuery(this).data('color'));
			}
		  });    

		}).scroll();
	
	
	
		// WOW ANIMATION 
			wow = new WOW(
			{
				animateClass: 'animated',
				offset:       50
			}
			);
			wow.init();
	
	
	
		// COUNTER
			 jQuery(document).scroll(function(){
				jQuery('.odometer').each( function () {
					var parent_section_postion = jQuery(this).closest('section').position();
					var parent_section_top = parent_section_postion.top;
					if (jQuery(document).scrollTop() > parent_section_top - 300) {
						if (jQuery(this).data('status') == 'yes') {
							jQuery(this).html( jQuery(this).data('count') );
							jQuery(this).data('status', 'no')
						}
					}
				});
			});
	
		
})(jQuery);

		