<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <section class="error-404 not-found">
            <div class="headlines">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
							<div class="slice200">
                            <h1> 404 </h1>
                            <p> The page you're looking for is not available. </p>
							<div class="custom-btn py-5 d-table m-auto " ><a href="<?php echo get_site_url() ?>">  Back to Home Page  <span></span> <i></i></a></div>
							</div>
                        </div>
                    </div>
                </div>
                <!-- end container -->
            </div>

        </section><!-- .error-404 -->

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>