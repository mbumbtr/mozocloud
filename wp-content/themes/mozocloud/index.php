<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php if ( is_home() && ! is_front_page() ) : ?>
				<div class="headlines">
					<div class="container">
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</div>
					<!-- end container -->
				</div>
			<?php endif; ?>

			<?php
			
			$i = 0;
			// Start the loop.
			while ( have_posts() ) :
				the_post();
				/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
				*/
				if($i == 0){
					get_template_part( 'template-parts/content', get_post_format() );
					$i++;					
				} else {
					get_template_part( 'template-parts/content-inner', get_post_format() );
				} 
				// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination(
				array(
					'prev_text'          => __( 'Previous ', 'twentysixteen' ),
					'next_text'          => __( 'Next ', 'twentysixteen' ),
					'before_page_number' => __( ' ', 'twentysixteen' ) . ' </span>',
				)
			);

			// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
